import discord
from discord.ext import commands

from ext.context import CustomContext

from collections import defaultdict
from contextlib import redirect_stdout

import datetime
import inspect
import psutil
import textwrap
import traceback

import io
import json
import os


class ModBot(commands.AutoShardedBot):
    '''
    A client for ModBot.
    '''

    def __init__(self, *args):
        super().__init__(command_prefix=self.prefix, owner_id=281821029490229251)
        self.uptime = datetime.datetime.utcnow()
        self.messages_sent = 0
        self.commands_used = defaultdict(int)
        self._extensions = [x.replace('.py', '') for x in os.listdir('cogs') if x.endswith('.py')]
        self.load_extensions()
        self.add_command(self.ping)
        self.add_command(self.shutdown)
        self.add_command(self._eval)
        self.add_command(self._bot)

    def load_extensions(self, cogs=None, path='cogs.'):
        for cog in cogs or self._extensions:
            members = inspect.getmembers(cog)
            for name, member in members:
                if name.startswith('on_'):
                    self.add_listener(member, name)
            try:
                self.load_extension(f'{path}{cog}')
            except Exception as e:
                print(f"LoadError: {path}{cog}\n{''.join(traceback.format_exception(type(e), e, e.__traceback__))}")

    @property
    def prefix(self):
        '''Returns the bot's prefix'''
        try:
            with open('data/config.json') as f:
                config = json.load(f)
                return ',.'
        except FileNotFoundError:
            return '?'

    @property
    def heroku(self):
        '''Checks if the bot is hosted on heroku'''
        try:
            with open('data/config.json') as f:
                config = json.load(f)
                return False
        except FileNotFoundError:
            return True

    @property
    def config(self):
        '''Returns the bot's config'''
        if self.heroku:
            return os.environ
        with open('data/config.json') as f:
            return json.load(f)

    @property
    def token(self):
        '''Returns the bot's token, wherever it is'''
        return self.config.get('token')

    @property
    def custom_emojis(self):
        '''Gets the emojis of servers that the bot is in.'''
        emojis = []
        for g in self.guilds:
            for e in g.emojis:
                emojis.append(e)
        return emojis

    @classmethod
    def run_bot(bot):
        '''Runs the actual bot'''
        bot = bot()
        try:
            bot.run(bot.token)
        except Exception as e:
            print(''.join(traceback.format_exception(type(e), e, e.__traceback__)))

    async def on_connect(self):
        '''Bot connected.'''
        print('-------------\nModBot connected!')

    async def on_ready(self):
        '''Bot startup.'''
        await self.change_presence(activity=discord.Game(name='with SBA!'))
        print(f'''
-------------
Bot is ready!
-------------
Author: SharpBit
-------------
Username: {self.user}
User ID: {self.user.id}
-------------
        ''')

    async def send_cmd_help(self, ctx):
        if ctx.invoked_subcommand:
            cmd = ctx.invoked_subcommand
        else:
            cmd = ctx.command
        em = discord.Embed(title=f'Usage: {ctx.prefix + cmd.signature}')
        em.color = discord.Color.red()
        em.description = cmd.help
        return em

    async def on_command_error(self, ctx, error):
        '''Invoked when there is a client or user-side error'''
        send_help = (commands.MissingRequiredArgument, commands.BadArgument, commands.TooManyArguments, commands.UserInputError)

        if isinstance(error, commands.CommandNotFound):
            pass

        elif isinstance(error, send_help):
            _help = await self.send_cmd_help(ctx)
            await ctx.send(embed=_help)

        elif isinstance(error, commands.CommandOnCooldown):
            await ctx.send(f'This command is on cooldown. Please wait {error.   retry_after:.2f}s')

        elif isinstance(error, commands.MissingPermissions):
            await ctx.send('You do not have the permissions to use this command.')
        else:
            print(''.join(traceback.format_exception(type(error), error, error.__traceback__)))

    async def on_command(self, ctx):
        '''Keeps track of how many times each command is used'''
        cmd = ctx.command.qualified_name.replace(' ', '_')
        self.commands_used[cmd] += 1

    async def process_commands(self, message):
        '''Uses the CustomContext class that inherits discord.Context'''
        ctx = await self.get_context(message, cls=CustomContext)
        if ctx.command is None:
            return
        await self.invoke(ctx)

    async def on_message(self, message):
        '''Records the number of messages sent and ignores bots.'''
        if message.author.bot:
            return
        self.messages_sent += 1
        await self.process_commands(message)

    @commands.command()
    async def ping(self, ctx):
        '''Pong! Returns your websocket latency.'''
        em = discord.Embed(
            title='Pong! Websocket Latency',
            description=f'{self.latency * 1000:.4f} ms',
            color=discord.Color.green()
        )

        await ctx.send(embed=em)

    @commands.command(name='bot')
    async def _bot(self, ctx):
        '''Returns info about the bot.'''
        em = discord.Embed(title='Bot Info', color=discord.Color.blue())
        em.set_author(name=self.bot.user, icon_url=ctx.guild.me.avatar_url)
        em.set_footer(text=f'Bot ID: {self.bot.user.id}')
        em.description = 'Bot stats for the bot.'
        em.timestamp = datetime.datetime.now()

        total_users = len(self.bot.users)
        total_online = len([m.id for m in self.bot.get_all_members() if m.status is not discord.Status.offline])

        now = datetime.datetime.utcnow()
        delta = now - self.bot.uptime
        hours, remainder = divmod(int(delta.total_seconds()), 3600)
        minutes, seconds = divmod(remainder, 60)
        days, hours = divmod(hours, 24)

        fmt = '{h}h {m}m {s}s'
        if days:
            fmt = '{d}d ' + fmt
        uptime = fmt.format(d=days, h=hours, m=minutes, s=seconds)

        memory_usage = (self.bot.process.memory_full_info().uss / 1024 ** 2) * 1.04858
        cpu_usage = self.bot.process.cpu_percent() / psutil.cpu_count()

        em.add_field(name='Guilds', value=len(self.bot.guilds))
        em.add_field(name='Channels', value=str(sum(1 for g in self.bot.guilds for _ in g.channels)))
        em.add_field(name='Online/Total Users', value=f'{total_online}/{total_users}')
        em.add_field(name='Uptime', value=uptime)
        em.add_field(name='Latency', value=f'{self.bot.latency * 1000:.2f} ms')
        em.add_field(name='Commands Run', value=sum(self.bot.commands_used.values()))
        em.add_field(name='Messages', value=self.bot.messages_sent)
        em.add_field(name='Memory Usage', value=f'{memory_usage:.2f} MB')
        em.add_field(name='CPU Usage', value=f'{cpu_usage}% CPU')

        await ctx.send(embed=em)

    @commands.command(name='eval')
    @commands.is_owner()
    async def _eval(self, ctx, *, body):
        '''Evaluates python code.'''
        env = {
            'ctx': ctx,
            'bot': self,
            'channel': ctx.channel,
            'author': ctx.author,
            'guild': ctx.guild,
            'message': ctx.message,
            'source': inspect.getsource
        }

        def cleanup_code(content):
            '''Automatically removes code blocks from the code.'''
            # remove ```py\n```
            if content.startswith('```') and content.endswith('```'):
                return '\n'.join(content.split('\n')[1:-1])

            # remove `foo`
            return content.strip('` \n')

        def get_syntax_error(e):
            if e.text is None:
                return f'```py\n{e.__class__.__name__}: {e}\n```'
            return f'```py\n{e.text}{"^":>{e.offset}}\n{e.__class__.__name__}: {e}  ```'

        env.update(globals())

        body = cleanup_code(body)
        stdout = io.StringIO()
        err = out = None

        to_compile = f'async def func():\n{textwrap.indent(body, "  ")}'

        def paginate(text: str):
            '''Simple generator that paginates text.'''
            last = 0
            pages = []
            for curr in range(0, len(text)):
                if curr % 1980 == 0:
                    pages.append(text[last:curr])
                    last = curr
                    appd_index = curr
            if appd_index != len(text) - 1:
                pages.append(text[last:curr])
            return list(filter(lambda a: a != '', pages))

        try:
            exec(to_compile, env)
        except Exception as e:
            err = await ctx.send(f'```py\n{e.__class__.__name__}: {e}\n```')
            return await ctx.message.add_reaction('\u2049')

        func = env['func']
        try:
            with redirect_stdout(stdout):
                ret = await func()
        except Exception as e:
            value = stdout.getvalue()
            err = await ctx.send(f'```py\n{value}{traceback.format_exc()}\n```')
        else:
            value = stdout.getvalue()
            if ret is None:
                if value:
                    try:

                        out = await ctx.send(f'```py\n{value}\n```')
                    except:
                        paginated_text = paginate(value)
                        for page in paginated_text:
                            if page == paginated_text[-1]:
                                out = await ctx.send(f'```py\n{page}\n```')
                                break
                            await ctx.send(f'```py\n{page}\n```')
            else:
                try:
                    out = await ctx.send(f'```py\n{value}{ret}\n```')
                except:
                    paginated_text = paginate(f"{value}{ret}")
                    for page in paginated_text:
                        if page == paginated_text[-1]:
                            out = await ctx.send(f'```py\n{page}\n```')
                            break
                        await ctx.send(f'```py\n{page}\n```')

        if out:
            await ctx.message.add_reaction('\u2705')  # tick
        elif err:
            await ctx.message.add_reaction('\u2049')  # x
        else:
            await ctx.message.add_reaction('\u2705')

    @commands.command(aliases=['restart'])
    @commands.is_owner()
    async def shutdown(self, ctx):
        '''Shuts down/restarts the bot.'''
        await ctx.send('Shutting down...')
        await self.logout()


if __name__ == '__main__':
    ModBot.run_bot()
