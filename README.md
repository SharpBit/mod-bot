# mod-bot
A moderation discord bot to practice event listeners.<br>
(also a custom bot for [SharpBit International Airport (SBA)](https://discord.gg/3NTS743))

### Hosting
It can be hosted on [heroku](https://heroku.com) or locally using a file in `data/config.json`<br>
the json file should look like this:
```json
{
    "token": "bot_token_here"
}
```
On heroku, connect it to the github repo and in settings>config vars add this:<br>
```
KEY: token
VALUE: bot_token_here
```
Then go to resources>worker and turn it on<br><br>
It should be online in a minute or two saying `Playing for SBA!` as the presence.
