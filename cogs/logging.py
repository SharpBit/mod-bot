import discord

from ext.utils import r


class Logging:
    '''
    Event listeners for logging moderator actions.
    '''

    def __init__(self, bot):
        self.bot = bot

    async def on_raw_reaction_add(self, data):
        guild = self.bot.get_guild(360531737526140929)
        reaction_guild = self.bot.get_guild(data.guild_id)
        passenger = r(reaction_guild, 367040811575148555)
        lounge = discord.utils.get(guild.channels, id=360531737526140930)
        member = guild.get_member(data.user_id)

        if data.message_id != 430398278186696704 or passenger in member.roles:
            return

        await member.add_roles(passenger, reason=f'{member.name} - Security Passed')
        await lounge.send(f'{member.mention} just passed security! Welcome to SBA, you may now proceed to your boarding gate.')

    async def on_member_join(self, member):
        guild = self.bot.get_guild(360531737526140929)
        check_in_channel = discord.utils.get(guild.channels, id=430808735842041868)

        await check_in_channel.send(
            f'Welcome {member.mention} to **SharpBit International Airport!** '
            'Read <#360531982955839498> and be sure to react to the message IN THAT CHANNEL with :thumbup::skin-tone-2: (not this channel). '
            'If you have already reacted to the message previously, just remove your reaction then add it again. '
            'Now you have checked-in your bags and passed security. '
            'Remember, only 1 carry-on and 1 personal bag is allowed per traveler. Hope you have a safe trip!'
        )

    async def on_message_delete(self, message):
        with open('./data/messages.txt', encoding='utf8') as f:
            messages = list(f)
            for i, m in enumerate(messages):
                messages[i] = int(m.strip('\n'))

        if message.guild.id != 360531737526140929 or message.channel.id == 377531520505741315 or message.author == self.bot.user or message.id in messages:
            return
        em = discord.Embed(title='Message Deleted', color=discord.Color.red())
        em.set_author(name=f'{message.author.name}#{message.author.discriminator} ({message.author.id})', icon_url=message.author.avatar_url)
        em.add_field(name='Message ID', value=message.id, inline=True)
        em.add_field(name='Channel', value=f'{message.channel} ({message.channel.id})', inline=True)
        em.add_field(name='Content', value=message.content, inline=False)
        await message.guild.get_channel(377531520505741315).send(embed=em)

    async def on_message_edit(self, before, after):
        if before.guild.id != 360531737526140929 or before.channel.id == 377531520505741315 or before.author == self.bot.user or before.content == after.content:
            return
        em = discord.Embed(title='Message Edited', color=discord.Color.blue())
        em.set_author(name=f'{before.author.name}#{before.author.discriminator} ({before.author.id})', icon_url=before.author.avatar_url)
        em.add_field(name='Message ID', value=after.id, inline=True)
        em.add_field(name='Channel', value=f'{before.channel} ({before.channel.id})', inline=True)
        em.add_field(name='Old Content', value=before.content, inline=False)
        em.add_field(name='New Content', value=after.content, inline=False)
        await before.guild.get_channel(377531520505741315).send(embed=em)

    async def on_guild_channel_create(self, channel):
        em = discord.Embed(title='Channel Created', description=f'Channel #{channel.name} was created.', color=discord.Color.green())
        em.set_footer(text=f'Channel ID: {channel.id}')
        await channel.guild.get_channel(377531520505741315).send(embed=em)

    async def on_guild_channel_delete(self, channel):
        em = discord.Embed(title='Channel Deleted', description=f'Channel #{channel.name} was deleted.', color=discord.Color.red())
        em.set_footer(text=f'Channel ID: {channel.id}')
        await channel.guild.get_channel(377531520505741315).send(embed=em)

    async def on_member_ban(self, member):
        em = discord.Embed(title='Member Banned', description=f'{member} ({member.id}) was banned from the guild.', color=discord.Color.red())
        await self.bot.get_guild(360531737526140929).get_channel(377531520505741315).send(embed=em)

    async def on_member_unban(self, user):
        em = discord.Embed(title='User Unbanned', description=f'{user} ({user.id}) was unbanned from the guild.', color=discord.Color.green())
        await self.bot.get_guild(360531737526140929).get_channel(377531520505741315).send(embed=em)

    # async def on_event_here(self, *args):
    #     async for entry in guild.audit_logs(limit=1):
    #         print(f'{entry.user} did {entry.action} to {entry.target}')


def setup(bot):
    bot.add_cog(Logging(bot))
