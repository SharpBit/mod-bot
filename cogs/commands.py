import discord
from discord.ext import commands

from ext.paginator import PaginatorSession
from ext.utils import role, r

import asyncio


class Moderation:
    '''Moderation commands for a guild'''

    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    @commands.has_permissions(kick_members=True)
    @commands.bot_has_permissions(kick_members=True)
    async def kick(self, ctx, user: discord.Member, *, reason=None):
        '''Kick a member from the guild'''
        await ctx.guild.kick(user, reason=f'{ctx.author.name} - {reason}')
        await ctx.send(f'Done. {user.name} was kicked from the guild.')

    @commands.command()
    @commands.has_permissions(ban_members=True)
    @commands.bot_has_permissions(ban_members=True)
    async def ban(self, ctx, user: discord.Member, *, reason=None):
        '''Ban a member from the guild'''
        await ctx.guild.ban(user, reason=f'{ctx.author.name} - {reason}')
        await ctx.send(f'Done. {user.name} was banned from the guild.')

    @commands.command()
    @commands.has_permissions(ban_members=True)
    @commands.bot_has_permissions(ban_members=True)
    async def unban(self, ctx, name_or_id, *, reason=None):
        '''Unban a member from the guild'''
        ban = await ctx.get_ban(name_or_id)
        if not ban:
            return await ctx.send('No user found.')
        await ctx.guild.unban(ban.user, reason=f'{ctx.author.name} - {reason}')
        await ctx.send(f'Unbanned *{ban.user}* from the guild.')

    @commands.command()
    @role(360535556117102592)
    @commands.bot_has_permissions(ban_members=True)
    async def bans(self, ctx):
        '''Gets the bans in the guild'''
        pages = []
        bans = await ctx.guild.bans()
        if len(bans) == 0:
            return await ctx.send('This guild has no bans.')
        for ban in bans:
            em = discord.Embed(title=f'{ban.user.name}#{ban.user.discriminator} ({ban.user.id})', description=ban.reason)
            em.set_thumbnail(url=ban.user.avatar_url)
            pages.append(em)

        await PaginatorSession(ctx, pages=pages).run()

    @commands.command()
    @commands.has_permissions(ban_members=True)
    @commands.bot_has_permissions(ban_members=True)
    async def softban(self, ctx, member: discord.Member, *, reason=None):
        '''Kicks a members and deletes their messages.'''
        await member.ban(reason=f'Softban - {reason}')
        await member.unban(reason='Softban unban.')
        await ctx.send(f'{member.name} was softbanned from the guild.')

    @commands.command()
    @commands.has_permissions(ban_members=True)
    @commands.bot_has_permissions(ban_members=True)
    async def hackban(self, ctx, user_id: int, *, reason=None):
        '''Bans a user that is currently not in the server.
        Only accepts user IDs.
        '''
        await ctx.guild.ban(discord.Object(id=user_id), reason=f'{ctx.author.name} - {reason}')
        await ctx.send(f'*{self.bot.get_user(user_id)}* just got hackbanned from the guild!')

    @commands.command()
    @role(360535556117102592)
    @commands.bot_has_permissions(manage_channels=True)
    async def mute(self, ctx, user: discord.Member, time: int=15):
        '''Mute a member in the guild for a certain number of minutes'''
        secs = time * 60
        await user.add_roles(r(360536859191017472), reason=f'{ctx.author.name} - Mute for {time}m')
        await ctx.send(f"{user.mention} has been muted for {time} minutes.")
        em = discord.Embed(
            title='Mute',
            description=f'{user} ({user.id}) has been muted by {ctx.author} for {time} minutes.',
            color=discord.Color.red()
        )
        await ctx.guild.get_channel(377531520505741315).send(embed=em)
        await asyncio.sleep(secs)
        await user.remove_roles(r(360536859191017472), reason=f'{ctx.author.name} - Unmute')
        await ctx.send(f'{user.mention} has been unmuted from the guild.')
        em = discord.Embed(
            title='Unmute',
            description=f'{user} ({user.id}) has been unmuted (Mute time is up)',
            color=discord.Color.green()
        )
        await ctx.guild.get_channel(377531520505741315).send(embed=em)

    @commands.command()
    @role(360535556117102592)
    @commands.bot_has_permissions(manage_channels=True)
    async def unmute(self, ctx, user: discord.Member):
        '''Unmute a member in the guild'''
        await user.remove_roles(r(360536859191017472), reason=f'{ctx.author} - Unmute')
        await ctx.send(f'{user.mention} has been unmuted from the guild.')
        em = discord.Embed(
            title='Unmute',
            description=f'{user} ({user.id}) has been unmuted by {ctx.author}',
            color=discord.Color.green()
        )
        await ctx.guild.get_channel(377531520505741315).send(embed=em)

    @commands.command()
    @role(360535556117102592)
    async def warn(self, ctx, user: discord.Member, *, reason: str=None):
        '''Warn a member via DMs'''
        warning = f'You have been warned in **{ctx.guild}** by **{ctx.author}** for {reason}'
        if not reason:
            warning = f'You have been warned in **{ctx.guild}** by **{ctx.author}**'
        try:
            await user.send(warning)
        except discord.Forbidden:
            return await ctx.send('The user has disabled DMs from the guild or blocked the bot.')
        else:
            await ctx.send(f'**{user}** has been **warned**')
            em = discord.Embed(
                title='Warn',
                description=f'{user} ({user.id}) has been warned by {ctx.author} ({ctx.author.id}) for {reason}',
                color=discord.Color.red()
            )
            await ctx.guild.get_channel(377531520505741315).send(embed=em)

    @commands.command()
    @commands.has_permissions(manage_messages=True)
    @commands.bot_has_permissions(manage_messages=True)
    async def purge(self, ctx, messages: int, reason: str=None):
        '''
        Delete messages a certain number of messages from a channel.
        Maximum is 300.
        '''
        if messages > 300:
            messages = 300
        message_ids = list()

        async for msg in ctx.channel.history(limit=messages + 1):
            message_ids.append(str(msg.id))
        with open('./data/messages.txt', 'w', encoding='utf8') as f:
            f.write('\n'.join(message_ids))
        await ctx.message.delete()

        if messages > 100:
            left_over = messages % 100
            for i in range(messages // 100):
                await ctx.channel.purge(limit=100)
                await asyncio.sleep(0.2)
            await ctx.channel.purge(limit=left_over)
        else:
            await ctx.channel.purge(limit=messages)

        await ctx.send(f'{ctx.author.mention}, {messages} messages deleted. 👌', delete_after=3)
        em = discord.Embed(
            title='Purge',
            color=discord.Color.blue()
        )

        if reason:
            em.description = f'{ctx.author} ({ctx.author.id}) purged {messages} messages in #{ctx.channel} for {reason}'
        else:
            em.description = f'{ctx.author} ({ctx.author.id}) purged {messages} messages in #{ctx.channel}'

        await ctx.guild.get_channel(377531520505741315).send(embed=em)


def setup(bot):
    bot.add_cog(Moderation(bot))
