import discord
from discord.ext import commands


def role(role_id):
    def wrapper(ctx):
        role = discord.utils.get(ctx.guild.roles, id=role_id)
        if ctx.author.top_role > role:
            return True
        return False
    return commands.check(wrapper)


def e(ctx, name):
    '''Helper function to get an emoji.'''
    return discord.utils.get(ctx.bot.custom_emojis, name=name)


def r(obj, _id):
    '''Helper function to get a role in the guild.'''
    return discord.utils.get(obj.guild.roles, id=_id)
