from discord.ext import commands


class CustomContext(commands.Context):

    async def get_ban(self, name_or_id):
        '''Get a ban in a guild.'''
        for ban in await self.guild.bans():
            if name_or_id.isdigit():
                if ban.user.id == int(name_or_id):
                    return ban
            if str(ban.user).lower().startswith(name_or_id.lower()):
                return ban
